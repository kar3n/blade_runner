package runner;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunnerFrame extends JFrame implements ActionListener {

  private Icon runningImage = new ImageIcon("src/main/resources/run.gif");
  private Icon standingImage = new ImageIcon("src/main/resources/noRun.png");
  private JLabel firstRunner = new JLabel(standingImage);
  private JLabel secondRunner = new JLabel(standingImage);
  private JLabel thirdRunner = new JLabel(standingImage);
  private JButton startButton = new JButton("START");
  
  RunnerFrame() {

    // COMPONENTS
    startButton.setBounds(265, 30, 100, 20);
    firstRunner.setBounds(50, 80, 55, 66);
    secondRunner.setBounds(50, 190, 55, 66);
    thirdRunner.setBounds(50, 310, 55, 66);

    // ADDS
    add(startButton);
    // add(firstRunner);
    getContentPane().add(firstRunner);
    add(secondRunner);
    add(thirdRunner);

    // ACTION LISTENERS
    startButton.addActionListener(this);

    // FRAME
    setLayout(null);
    setResizable(false);
    setSize(640, 480);
    setLocationRelativeTo(null);
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {

    firstRunner.setIcon(runningImage);
    secondRunner.setIcon(runningImage);
    thirdRunner.setIcon(runningImage);

    Thread t1 = new Thread(() -> {
      for (int i = 50; true; i++) {
        firstRunner.setLocation(i, firstRunner.getY());
        try {
          Thread.sleep(new Random().nextInt(30));
        }
        catch (InterruptedException e1) {
          e1.printStackTrace();
        }
        if (i == 526 && secondRunner.getX() != 526 && thirdRunner.getX() != 526) {
          firstRunner.setIcon(standingImage);
          JOptionPane.showMessageDialog(null, "First runner wins");
          break;
        }
        else if (secondRunner.getX() == 526 || thirdRunner.getX() == 526) {
          firstRunner.setIcon(standingImage);
          break;
        }
      }
    });
  
    Thread t2 = new Thread(() -> {
      for (int i = 50; true; i++) {
        secondRunner.setLocation(i, secondRunner.getY());
        try {
          Thread.sleep((new Random().nextInt(30)));
        }
        catch (InterruptedException e13) {
          e13.printStackTrace();
        }
        if (i == 526 && firstRunner.getX() != 526 && thirdRunner.getX() != 526) {
          secondRunner.setIcon(standingImage);
          JOptionPane.showMessageDialog(null, "Second runner wins");
          break;
        }
        else if (firstRunner.getX() == 526 || thirdRunner.getX() == 526) {
          secondRunner.setIcon(standingImage);
          break;
        }
      }
    });
  
    Thread t3 = new Thread(() -> {
      for (int i = 50; true; i++) {
        thirdRunner.setLocation(i, thirdRunner.getY());
        try {
          if (Thread.interrupted()) {
            return;
          }
          Thread.sleep(new Random().nextInt(30));
        }
        catch (InterruptedException e12) {
          e12.printStackTrace();
        }
        if (i == 526 && firstRunner.getX() != 526 && secondRunner.getX() != 526) {
          thirdRunner.setIcon(standingImage);
          JOptionPane.showMessageDialog(null, "Third runner wins");
          break;
        }
        else if (firstRunner.getX() == 526 || secondRunner.getX() == 526) {
          thirdRunner.setIcon(standingImage);
          break;
        }
      }
    });
  
    ExecutorService executorService = Executors.newFixedThreadPool(3);
    executorService.submit(t1);
    executorService.submit(t2);
    executorService.submit(t3);

    startButton.removeActionListener(this);
  }

  @Override
  public void paint(Graphics g) {
    super.paint(g);
    for (int j = 0; true; j += 15) {
      if (j > 15) break;
      for (int i = 0; true; i += 15) {
        if (i == 165) break;
        g.fillRect(590 + j, 110 + 2 * i + j, 15, 15);
        g.fillRect(605 - j, 110 + 21 * j, 15, 15);
      }
    }
  }
}
